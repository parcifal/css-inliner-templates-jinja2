#!/usr/bin/env node

const fs = require("fs");

const CSSInliner = require("css-inliner");
const CSSInlinerTemplateJinja2 = require("../");

const inliner = new CSSInliner({
    directory: "test",
    template: CSSInlinerTemplateJinja2
});

const html = fs.readFileSync("test/test.jinja2");

inliner.inlineCSSAsync(html).then((result) => {
    fs.writeFileSync("test/test.css.jinja2", result);
});
