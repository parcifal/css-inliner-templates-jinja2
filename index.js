
module.exports = (template) => {
    return template.match(new RegExp([
        /**
         * Trans block, prevents HTML encoding in translated strings.
         *
         *  > {% trans bacon="eggs" %}
         *  >     spam %(bacon)s
         *  > {% endtrans }
         */
        /{%\s*trans(?:.(?!%}))*.%}(?:.(?!{%\s*endtrans\s*%}))*.{%\s*endtrans\s*%}/,

        /**
         * Statements.
         *
         *  > {{ "foo" }}
         */
        /{{(?:.(?!}}))*.}}/,

        /**
         * Expressions and blocks.
         *
         *  > {% if this %}
         *  >     ...
         *  > {% endif %}
         */
        /{%(?:.(?!%}))*.%}/,

        /**
         * Comments.
         *
         *  > {# Spam, bacon, eggs. #}
         */
        /{#(?:.(?!#}))*.#}/
    ].map(p => p.source).join("|"), "gms"));
};
